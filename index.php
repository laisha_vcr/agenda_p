<?php
include("conexion.php");
$con = conectar();
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <title>Agenda Comercial</title>
    <style>
    .content {
        margin-top: 80px;
    }
    </style>

</head>

<body>

    <div class="container">
        <div class="content">
            <h1 align="center">Agenda Comercial</h1>
            <a href="add.php" class="btn btn-sm btn-success">+ Agregar</a>
            <hr />

            <?php
			if(isset($_GET['sup']) == 'delete'){
		
				$nik = mysqli_real_escape_string($con,(strip_tags($_GET["nik"],ENT_QUOTES)));
				$cek = mysqli_query($con, "SELECT * FROM contacto WHERE id_ct='$nik'");
				if(mysqli_num_rows($cek) == 0){
					echo '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="ale
                    rt" aria-hidden="true">&times;</button> No se encontraron datos.</div>';
				}else{
					$delete = mysqli_query($con, "DELETE FROM contacto WHERE id_ct='$nik'");
					if($delete){
						echo '<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Datos eliminado correctamente.</div>';
					}else{
						echo '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error, no se pudo eliminar los datos.</div>';
					}
				}
			}
			?>


            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <tr>
                        <td> </td>
                        <td>Nombre</td>
                        <td>Domicilio</td>
                        <td>Servicio</td>
                        <td>Telefono</td>
                        <td>Descripcion</td>


                      
                    </tr>
                    <?php
                
					$sql = mysqli_query($con, "SELECT * FROM contacto ORDER BY nombre_ct ASC");

				if(mysqli_num_rows($sql) == 0){
					echo '<tr><td colspan="8">No hay datos.</td></tr>';
				}else{
				
					while($mostrar = mysqli_fetch_assoc($sql)){
						echo '
						<tr>
							<td>  </td>

                            <td>'.$mostrar['nombre_ct'].'</td>
                            <td>'.$mostrar['domicilio_ct'].'</td>
							<td>'.$mostrar['servicio_ct'].'</td>
                            <td>'.$mostrar['telefono_ct'].'</td>
                            <td>'.$mostrar['descripcion_ct'].'</td>
							<td>';
							
						echo '
							</td>
							<td>

								<a href="edit.php?nik='.$mostrar['id_ct'].'" title="Editar datos" class="btn btn-primary btn-m">Editar</a><br>
								
                                <a href="index.php?sup=delete&nik='.$mostrar['id_ct'].'" title="Eliminar" onclick="return confirm(\'Esta seguro de borrar los datos '.$mostrar['nombre_ct'].'?\')" class="btn btn-danger btn-m">Eliminar</a>
                                
                                </td>
						</tr>
						';
						
					}
				}
				?>
                </table>
            </div>
        </div>
    </div>

</body>

</html>