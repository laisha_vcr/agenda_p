<?php
include("conexion.php");
$con = conectar();
?>
<!DOCTYPE html>
<html lang="es">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Informacion de contactos</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-datepicker.css" rel="stylesheet">
	<link href="css/style_nav.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <title>Editar contacto</title>
	<style>
		.content {
			margin-top: 80px;
		}
	</style>
	

</head>
<body>

	<div class="container">
		<div class="content">
		<a href="index.php" ><i class="fas fa-undo-alt">Regresar</i></a>
			<h2>Datos del Contacto &raquo; Editar datos</h2>
			<hr />
			
			<?php

			$nik = mysqli_real_escape_string($con,(strip_tags($_GET["nik"],ENT_QUOTES)));
			$sql = mysqli_query($con, "SELECT * FROM contacto WHERE id_ct='$nik'");
			if(mysqli_num_rows($sql) == 0){
				header("Location: index.php");
			}else{
				$row = mysqli_fetch_assoc($sql);
			}
			if(isset($_POST['save'])){
				$id_ct		     = mysqli_real_escape_string($con,(strip_tags($_POST["id_ct"],ENT_QUOTES)));//Escanpando caracteres 
				$nombre_ct		     = mysqli_real_escape_string($con,(strip_tags($_POST["nombre_ct"],ENT_QUOTES)));//Escanpando caracteres 
				$domicilio_ct	 = mysqli_real_escape_string($con,(strip_tags($_POST["domicilio_ct"],ENT_QUOTES)));//Escanpando caracteres 
				$servicio_ct	 = mysqli_real_escape_string($con,(strip_tags($_POST["servicio_ct"],ENT_QUOTES)));//Escanpando caracteres 
				$telefono_ct	     = mysqli_real_escape_string($con,(strip_tags($_POST["telefono_ct"],ENT_QUOTES)));//Escanpando caracteres 
				$descripcion_ct		 = mysqli_real_escape_string($con,(strip_tags($_POST["descripcion_ct"],ENT_QUOTES)));//Escanpando caracteres 
			
				
				$update = mysqli_query($con, "UPDATE Contacto SET nombre_ct='$nombre_ct', domicilio_ct='$domicilio_ct', servicio_ct='$servicio_ct', telefono_ct='$telefono_ct', descripcion_ct='$descripcion_ct' WHERE id_ct='$nik'") or die(mysqli_error());
				if($update){
					header("Location: edit.php?nik=".$nik."&pesan=sukses");
				}else{
					echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.</div>';
				}
			}
			
			if(isset($_GET['pesan']) == 'sukses'){
				echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados.</div>';
			}
			?>
			<form class="form-horizontal" action="" method="post">
				<div class="form-group">
					<label class="col-sm-3 control-label">ID</label>
					<div class="col-sm-2">
						<input disabled type="text" name="id_ct" value="<?php echo $row ['id_ct']; ?>" class="form-control" placeholder="ID" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Nombre</label>
					<div class="col-sm-4">
						<input type="text" name="nombre_ct" value="<?php echo $row ['nombre_ct']; ?>" class="form-control" placeholder="Nombre" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Domicilio</label>
					<div class="col-sm-4">
						<input type="text" name="domicilio_ct" value="<?php echo $row ['domicilio_ct']; ?>" class="form-control" placeholder="Domicilio" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Servicio</label>
					<div class="col-sm-4">
						<input type="text" name="servicio_ct" value="<?php echo $row ['servicio_ct']; ?>" class="input-group date form-control" date="" data-date-format="yyyy-mm-dd" placeholder="0000-00-00" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Telefono</label>
					<div class="col-sm-3">
						<input type="tel" name="telefono_ct" value="<?php echo $row ['telefono_ct']; ?>" class="form-control" placeholder="Telefono" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Descripcion</label>
					<div class="col-sm-3">
					<textarea name="descripcion_ct" class="form-control" placeholder="Descripcion"><?php echo $row ['descripcion_ct']; ?></textarea>
					</div>
				</div>
		

				<div class="form-group">
					<label class="col-sm-3 control-label">&nbsp;</label>
					<div class="col-sm-6">
						<input type="submit" name="save" class="btn btn-sm btn-primary" value="Guardar datos">
						<a href="index.php" class="btn btn-sm btn-danger">Cancelar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

</body>
</html>